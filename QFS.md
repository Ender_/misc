#QFS Draft:
-------------------------


QFS is a file system made to retain simplicity while not having the issues of FAT32.
It is partially inspired by [SFS](http://http://wiki.osdev.org/SFS).

A QFS partition is split into 4 sections:

```
x------------------------------------------------------------x
|         Boot sector and partition info.                    |
x------------------------------------------------------------x
|                           Reserved                         |
x------------------------------------------------------------x
|                             Data                           |
x------------------------------------------------------------x
|                     Free space (I hope)                    |
x------------------------------------------------------------x
|                  Metadata (grows upwards)                  |
x------------------------------------------------------------x
```

The first sector contains the boot-code (if used on a bootable partition) and a small amount of FS information:

```
>0x01??: Checksum of 0x01D4 to 0x01FE
>0x01D4: Magic word "MAGIC!"
>0x01DA: Partition name (HELLO WORLD! length) (pad with whatever you want, but it needs to be 12 bytes long)
>0x01E6: End of data segment/Start of free space
>0x01EE: End of metadata/End of free space
>0x01F6: End of reserved space/Start of data
>0x01FE: 0xAA55
>0x0200: END OF SECTOR
```

The reserved area can contain whatever you want, and should be ignored by the FS driver.


The data area contains all the files.  All the metadata is contained in the metadata section.

The metadata section grows upwards instead of downwards, so the 'top' of it is the end of the disk/partition.

The structure is as follows:

```
x------------------------------------------------------------x
|                        Timestamp                           |
x------------------------------------------------------------x
|                      File metadata                         |
x------------------------------------------------------------x
```

It's pretty simple.  The file metadata is as follows:

```
x------------------------------------------------------------x
|                Filename (Null terminated)                  |
x------------------------------------------------------------x
|                          Timestamp                         |
x------------------------------------------------------------x
|                Physical location (Special terminated)         |
x------------------------------------------------------------x
|                          Directory                         |
x------------------------------------------------------------x
|                         Permissions                        |
x------------------------------------------------------------x
|                          End token                         |
x------------------------------------------------------------x
```

The end token shows that the file metadata has ended, and there is a new file next.

The physical location is stored like this:
```START: Head:Track:Sector:Offset ENDSTART - END: Head:Track:Sector:Offset DONE```
For fragmented files it is stored like this (1 split):

```
START (0x01): Head:Track:Sector:Offset
ENDSTART 0x200
Pause (0x02): Head:Track:Sector:Offset
ENDPAUSE 0x211
Resume (0x03): Head:Track:Sector:Offset
ENDRES 0x222
END (0x04): Head:Track:Sector:Offset
DONE 0x233
```

Less abstract with 1 split:

```
0x01 HEAD 0x00 0x00 TRACK 0x00 0x00 SECTOR 0x00 0x00 OFFSET 0x200
0x02 HEAD 0x00 0x00 TRACK 0x00 0x00 SECTOR 0x00 0x00 OFFSET 0x211
0x03 HEAD 0x00 0x00 TRACK 0x00 0x00 SECTOR 0x00 0x00 OFFSET 0x222
0x04 HEAD 0x00 0x00 TRACK 0x00 0x00 SECTOR 0x00 0x00 OFFSET 0x233
```

This allows for as large files as you want, unlike FAT32.