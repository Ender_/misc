#!/usr/bin/python
import sys, re

if (len(sys.argv) != 2):
	print("Usage:\t%s <hex input>" % sys.argv[0])
	exit(-1)

x = []
y = sys.argv[1].strip("0x").strip(' ').strip('h')


for z in reversed([y[i:i+2] for i in range(0, len(y), 2)]):
	x.append(z)

print(''.join(x))
print(' '.join(x))
print("0x"+''.join(x))
print("\\x"+"\\x".join(x))
print(''.join(x)+'h')
