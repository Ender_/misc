# EndianC[onverter]

A python script to convert hex input from big endian to little endian and vise versa.


```
Usage:	./endianc.py <hex input>
```

Examples:
```
$ ./endianc.py 0x4231
2143
21 43
0x2143
\x21\x43
2143h
```
```
$ ./endianc.py 4321
2143
21 43
0x2143
\x21\x43
2143h
```
```
$ ./endianc.py 4321h
2143
21 43
0x2143
\x21\x43
2143h
```
